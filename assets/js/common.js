/*!
 * http://coilbuildratio.com/
 *
 * Copyright 2016 Luqman Marzuki
 * Released under the MIT license
 * https://bitbucket.org/luqmanm/coilbuildratio.com/LICENSE.md
 */

/* Convert AWG to mm */
function awgtomm(awg){
	if( awg == 0 ) return 0;
	return (0.127 * Math.pow(92, ((36-awg)/39))).toFixed(3) ;
}

function populateOptionAWG(){
	var x = 18;
	var options = '';
	for(;x<=46;x+=2){
		options += '<option value="'+x+'">'+x+'</option>';
		if(x==26) options += '<option value="27">27</option>';
	}
	$(options).appendTo('select.populate-awg');
}

/* Calculate wrap length ratio */
function calcTsukaFramedStapleWrapLengthRatio(){
	/* input validations */
	if( ! $.isNumeric(params.core_ribbon_stack) || params.core_ribbon_stack < 1 ) return 0;
	if( ! $.isNumeric(params.core_length) || params.core_length <= 0 ) return 0;
	
	/* Define Parameters */
	var core_width = (params.core_ribbon_dimension_w * params.core_ribbon_stack) + (2 * params.core_frame_gauge) + params.wrap_ribbon_dimension_w;
	core_width = parseFloat(core_width).toFixed(3);

	if( params.core_ribbon_dimension_t > params.core_frame_gauge ){
		var core_height = params.core_ribbon_dimension_t + params.wrap_ribbon_dimension_w;
	}
	else{
		var core_height = params.core_frame_gauge + params.wrap_ribbon_dimension_w;
	}
	core_height = parseFloat(core_height).toFixed(3);

	var core_radius = ( params.core_frame_gauge / 2 ) + params.wrap_ribbon_dimension_w;
	core_radius = parseFloat(core_radius).toFixed(3);

	// Find 1 loop length
	var wrap_single_loop_length = 2 * ( (core_width * core_height) + (3.14 * core_radius) );

	// Total loops on the required distance
	var wrap_total_loops = params.core_length / params.wrap_ribbon_dimension_t;
	// mm length for the total loops
	var wrap_length = wrap_total_loops * wrap_single_loop_length;
	// Make the wrap length ratio
	var wrap_length_ratio = Math.ceil(wrap_length / params.core_length);

	// Tambahin ribbon wire twist compression
	var ribbon_twist_compression = Math.ceil(parseFloat(wrap_length_ratio / core_width).toFixed(1));

	return wrap_length_ratio + ribbon_twist_compression;
}

/* Calculate wrap length ratio */
function calcStapleWrapLengthRatio(){
	/* input validations */
	if( ! $.isNumeric(params.core_stack) || params.core_stack < 1 ) return 0;
	if( ! $.isNumeric(params.core_length) || params.core_length <= 0 ) return 0;

	/* Define Parameters */
	var core_width = (params.core_dimension_w * params.core_stack) + params.wrap_gauge;
	var core_height =  params.core_dimension_t + params.wrap_gauge;

	// Find 1 loop length
	var wrap_single_loop_length = 2 * ( core_width + core_height );
	// Plus the spacing
	wrap_single_loop_length = parseFloat(wrap_single_loop_length) + parseFloat(params.wrap_spacing);

	// Total loops on the required distance
	var wrap_total_loops = params.core_length / (params.wrap_spacing + params.wrap_gauge);
	// mm length for the total loops
	var wrap_length = wrap_total_loops * wrap_single_loop_length;
	// Make the wrap length ratio
	var wrap_length_ratio = Math.ceil(wrap_length / params.core_length);

	return wrap_length_ratio;
}

/* Calculate wrap length ratio */
function calcStaggeredFusedWrapLengthRatio(){
	/* input validations */
	if( ! $.isNumeric(params.core_length) || params.core_length <= 0 ) return 0;

	/* Define Parameters */
	var core_width = (2 * params.wrap_spacing ) + params.core_gauge + params.wrap_gauge;
	core_width = parseFloat(core_width).toFixed(3);

	var core_height = parseFloat(params.core_gauge + params.wrap_gauge).toFixed(3);

	var core_radius = ( params.core_gauge / 2 ) + params.wrap_gauge;
	core_radius = parseFloat(core_radius).toFixed(3);

	// Find 1 loop length
	var wrap_single_loop_length = 2 * ( (core_width * core_height) + (3.14 * core_radius) );
	// Plus the spacing
	wrap_single_loop_length = parseFloat(wrap_single_loop_length) + parseFloat(params.wrap_spacing);

	// Total loops on the required distance
	var wrap_total_loops = params.core_length / (params.wrap_spacing + params.wrap_gauge);
	// mm length for the total loops
	var wrap_length = wrap_total_loops * wrap_single_loop_length;
	// Make the wrap length ratio
	var wrap_length_ratio = Math.ceil(wrap_length / params.core_length);

	return wrap_length_ratio;
}

/* Calculate wrap length ratio for single core clapton */
function calcClaptonWrapLengthRatio(){
	/* input validations */
	if( ! $.isNumeric(params.core_length) || params.core_length <= 0 ) return 0;
	// Find 1 loop length
	var wrap_single_loop_length = 3.14 * ( params.core_gauge + params.wrap_gauge );
	// Plus the spacing
	wrap_single_loop_length = parseFloat(wrap_single_loop_length) + parseFloat(params.wrap_spacing);
	// Total loops on the required distance
	var wrap_total_loops = params.core_length / (params.wrap_spacing + params.wrap_gauge);
	// mm length for the total loops
	var wrap_length = wrap_total_loops * wrap_single_loop_length;
	// Make the wrap length ratio
	var wrap_length_ratio = Math.ceil(wrap_length / params.core_length);

	return wrap_length_ratio;
}